'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var errorHandler = require('../helpers/errorHandler');
var isDevMode = require('../helpers/envHelpers').isDevMode;

module.exports = function() {
  return gulp.src('app/*.jade')
    .pipe($.jade({
      basedir: '.',
      pretty: isDevMode()
    }))
    .on('error', errorHandler)
    .pipe(gulp.dest('build'));
};
